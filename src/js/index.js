import axios from 'axios';

let form = document.querySelector('.search__form'),
    searchResult = document.querySelector('.search__result'),
    searchMore = document.querySelector('.search__more'),
    loader = document.querySelector('.loader');

let formData = () => {
        let field = form.querySelector('input'),
            data = {};
        data[field.getAttribute('name')] = field.value;
        return data;
    },
    createImage = (image) => {
        let urlPhoto = `https://farm${image.farm}.staticflickr.com/${image.server}/${image.id}_${image.secret}.jpg`,
            img = `<img src="${urlPhoto}" alt="" class="result__img">`,
            block = document.createElement('div');
        block.classList.add('result__block');
        block.innerHTML = img;
        return block;
    },
    getPhoto = (page, search) => {
        let data = formData(),
            option = {
                'method': 'flickr.photos.search',
                'api_key': '56e7db74f14e3a1f5a5f2179149d7147',
                'format': 'json',
                'nojsoncallback': 1,
                'per_page': 10,
                'page': page
            };
        Object.assign(option, data);
        loader.classList.add('active');
        axios.get('https://www.flickr.com/services/rest/', {params: option})
            .then(function (response) {
                if (search) {
                    searchResult.innerHTML = '';
                    searchMore.dataset.id = 2;
                    searchMore.classList.remove('hidden');
                }
                let arrPhotos = response.data.photos.photo;
                if(arrPhotos.length < 10){
                    searchMore.classList.add('hidden');
                }
                arrPhotos.forEach(photo => {
                    searchResult.appendChild(createImage(photo));
                })
                loader.classList.remove('active');
            })
            .catch(function (error) {
                console.log(error)
                loader.classList.remove('active');
            })
    }

form.addEventListener('submit', (e) => {
    e.preventDefault();
    getPhoto(1, true);
})

searchMore.addEventListener('click', (e) => {
    let page = searchMore.dataset.id;
    getPhoto(page, false);
    searchMore.dataset.id = ++page;

});
